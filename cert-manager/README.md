# cert-manager
[cert-manager](https://cert-manager.readthedocs.io/) is an open source project for Kubernetes to help in managing certificates on your cluster. The most prominent feature is the ability for it to manage the creation and renewal of valid certificates for domains using Let's Encrypt.

## About the standard deployment
This deployment currently installs a basic helm chart with no modifications, accepting all of the defaults. It also includes some custom resource definitions (CRDs) that have been separated out of the helm chart, so be sure to update as necessary during the upgrade process.

In addition, a couple of ClusterIssuers are included as part of this deployment. The `letsencrypt-prod` ClusterIssuer is available to all namespaces within the cluster and it uses the http01 challenge type, meaning that as long as your domain resolves to an ingress controller on the cluster, a user should be able to get a valid Let's Encrypt certificate. The `letsencrypt-route53` ClusterIssuer works with our AWS and Cloudflare domains.

By default, this will be a 90 day cert that is renewed 30 days prior to expiration (so it is renewed every 60 days).

## Upgrading
Always check the [upgrade documentation](https://docs.cert-manager.io/en/latest/tasks/upgrading/index.html) to make sure there aren't specific tasks that need to be completed. You may also want to check the [GitHub releases](https://github.com/jetstack/cert-manager/releases) for notes, or the [Helm Hub](https://hub.helm.sh/charts/jetstack/cert-manager) page for updates to the chart versions.

In general, you may need to update the `crds-v*.yml` file with new CRD definitions as well as the `helm-deployment.yml` with new chart information.
